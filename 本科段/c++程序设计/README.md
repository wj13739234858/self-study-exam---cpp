<!--
 * @Author: Wj
 * @Date: 2023-11-08 22:05:32
 * @LastEditors: Wj
 * @LastEditTime: 2023-11-09 18:02:02
 * @Description: 请填写简介
-->

# 进度条

```mermaid
gantt
title c++页码进度🌰

dateFormat MM-DD

section 章节进度
35:crit,des1,11-08,02d
第二章:crit,des2,11-10,02d
```

```mermaid
gantt
title c++章节进度🌰

dateFormat MM-DD

section 章节进度
第一章:crit,des1,11-08,02d
第二章:crit,des2,11-10,02d
```

# 目录结构

```
c++程序设计
│  │  README.md                          描述进度和目录结构
│  │  单词本.md                           单词收集
│  │
│  ├─模拟卷                               全书学完的模拟卷
│  ├─第一章_c++语言简介                   章节目录
│  │  │  习题.md                         章节习题
│  │  │  知识结构.md                     章节知识结构总结
│  │  │
│  │  └─代码                             章节代码
│  │          1_1.cpp
│  └─第二章_面向对象的基本概念

```
# 环境配置
## 1.下载vscode

官网：https://code.visualstudio.com/
配置中文插件：Chinese (Simplified) (简体中文) Language 

## 2.下载编译器MinGW

官网：https://sourceforge.net/projects/mingw-w64/files/mingw-w64/mingw-w64-release/

## 3.配置环境变量

## 4.检查环境变量是否配置成功

cmd->gcc -v

## 5.vscode c++插件安装

c++插件：C/C++ Extension Pack

## 6.配置
g++编译

## 参考
https://zhuanlan.zhihu.com/p/137760796

## 注意
g++可以识别中文目录，gdb识别不了
所有其他代码均在demo文件夹下


## 拓展
学完c->c++ ->数据结构 ->操作系统后->linux0.11版源码只有几千行 （高等数学如果需要可以看看）
然后仿照写一个小的demo

后续可以看rtos->freertos ->可以仿照一个

后续可以看risc-v 然后进行移植，移植一个自己写的os

后续可以看特定的处理器结构跑特定的代码功能，进行优化

后续可以在特定的硬件结构上移植

## 过程感悟
本书主要学的是c++的标准库
还有很多三方库：https://blog.csdn.net/yuanlulu/article/details/108655614

## 提及软件
自由软件：https://www.gnu.org/software/software.html
```
gcc c编译器
grub bootloader引导
bash shell的cli
bc 计算器
fdisk 磁盘分盘
gdb c/c++调试
grep 文件检索
lrzsz linux传输
sed 文本修改
tar 打包
gzip 压缩
glibc linux下标准库
```
