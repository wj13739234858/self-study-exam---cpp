/*
 * @Author: Wj
 * @Date: 2023-11-10 16:27:33
 * @LastEditors: Wj
 * @LastEditTime: 2023-11-10 16:27:33
 * @Description: 请填写简介
 */
/*
 * @Author: Wj
 * @Date: 2023-11-10 09:12:22
 * @LastEditors: Wj
 * @LastEditTime: 2023-11-10 09:55:02
 * @Description: 定义带默认值的函数
 */

# include <iostream>
using namespace std;
void func(int a=1,int b=22,int c=33){ // 为参数a,b,c分别设置了默认值11,22,33
    cout<<"a="<<a<<"\tb="<<b<<"\tc="<<c<<endl; 
}
int main(){ 
    func(); // 调用时缺少了3个实参，将使用定义中的三个参数的默认值
    func(55); // 调用时缺少了后两个实参，将使用定义中的后两个参数值
    func(77,99); // 与上面类似
    func(8,88,888); //调用时实参完备，不使用定义中的任何参数默认值
    return 0;
}