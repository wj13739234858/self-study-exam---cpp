/*
 * @Author: Wj
 * @Date: 2023-11-08 22:13:12
 * @LastEditors: Wj
 * @LastEditTime: 2023-11-09 17:59:34
 * @Description:  输入/输出示例
 */

#include <iostream>   // 头文件： io 流库 包含输入流类 istream 以及输出流类ostream ,cin 以及cout分别是istream以及ostream的对象(单例)
// include 包含头文件，头文件，头文件一般用于保存声明，函数声明，常量声明等
#include <string>   // 字符串对象 后缀为.h的头文件c++已经明确不支持了
using namespace std;  //标准库类对象的标识符命名空间，，没有这个endl无法识别，cin，cout无法识别，避免名字定义冲突
int main(){
    int oneInt1,oneInt2;
    char strArray[20];
    string str; 
    double oneDouble;
    char oneChar='a';
    cout<<"输入两个整型值，一个字符，一个字符串和一个浮点数,";  // coout
    cout<<"以空格，Tab键或Enter键分隔:"<<endl; // endl代表换行
    // cin>>oneInt1>>oneInt2>>oneChar>>strArray>>oneDouble;
    cin>>oneInt1>>oneChar>>oneInt2>>strArray>>oneDouble; //  << 或者>> 后面只能跟一个项或者一个表达式
    str=strArray; // 字符串的本质就是字符数组
    cout<< "123232343241234213\
34325324523453"<<endl;   // 如果字符串过长可以用\分为多行
    cout<<"输入的数据是:"<<endl;
    cout<<"字符串是:\t\t"<<str<<endl
        <<"两个整型值分别是:\t"<<oneInt1<<"和\t"<<oneInt2<<endl // \t是制表符，4个空格
        <<"字符是：\t\t"<<oneChar<<"\n"
        <<"浮点值是：\t\t"<<oneDouble<<endl;
    return 0;
 }
                                                                                                                                 
// 注意：cin接收不了 空格，回车，制表符
// 需要接收用getchar()函数