/*
 * @Author: Wj
 * @Date: 2023-11-09 18:26:44
 * @LastEditors: Wj
 * @LastEditTime: 2023-11-10 09:08:15
 * @Description: 强制类型转换示例
 */
# include <iostream>
using namespace std;
int main(){
    int a = 10;
    const int *p=&a;  // *p是定义时指针的表示，如果单独拿来用表示*是间接取址操作符，表示间接通过p中的地址取到a中的数据 p中存放的是地址，&是取地址符，&a表示取a所在的数据的地址
                        // 整句话的意思就是取a代表数据的地址给p
                        // 不能使用常量指针p修改a的值，const表示常量，即*p是个常量，这个地址不可以改变
                        // *p = 40 类似这种间接赋值的方法进行赋值
    const int ca=30; // 被const修饰  const表示常量
    int *q;  // 定义指针*q   指针是一组地址也是一组整数，可以用int来定义指针
    cout<<"a的地址为：\t"<<&a<<"\ta的值为：\t"<<a<<endl;
    cout<<"*p指向的地址为："<<p<<"\t*p的值为：\t"<<*p<<endl;
    q=const_cast<int*>(p) ;// 指针的常量赋值和变量赋值不同，不能直接q=p 这叫去除p的常量性赋给q
    *q=20; // 如果写*p=20是错误的，因为*p是常量  ？？？代表什么
    cout<<"a的地址为:\t"<<&a<<"\ta的值为：\t"<<a<<endl;
    cout<<"*p指向的地址为："<<p<<"\t*p的值为：\t"<<*p<<endl;
    cout<<"*q指向的地址为："<<q<<"\t*q的值为：\t"<<*q<<endl;
    cout<<"分界线"<<endl;
    p=&ca; //ca的值不能修改
    q=const_cast<int*>(p); // 去除p的常量性赋给q，如果写q=p会报错
    // 也可也理解为如何将常量指针赋给变量指针，去常量化赋值
    *q=40; //间接赋值，赋值指向指针常量定义的内存单元，前提是去除p的常量性赋给q
    // 也可以理解为如何修改常量在不改变常量定义的情况下
    cout<<"ca的地址为:\t"<<&ca<<"\tca的值为:\t"<<ca<<endl;
    cout<<"*p指向的地址为:"<<p<<"\t*p的值为:\t"<<*p<<endl;
    cout<<"*q指向的地址为:"<<q<<"\t*q的值为:\t"<<*q<<endl;
    return 0;
}