/*
 * @Author: Wj
 * @Date: 2023-11-10 16:37:37
 * @LastEditors: Wj
 * @LastEditTime: 2023-11-10 17:01:02
 * @Description: 引用的定义及使用
 */
# include <iostream>
using namespace std;
int main(){
    int oneInt=1; // 定义普通变量并初始化
    int& ref = oneInt; //为普通变量oneInt设置别名(普通引用)，ref指向与oneInt同一块内存空间
    const int& refc=oneInt;//为普通变量oneInt设置别名(常引用)，refc指向与oneInt同一块内存空间
    ref=2; // 修改ref所在内存，赋值为2
    cout<<"oneInt="<<oneInt<<"\t"<<"ref="<<ref<<endl;
    cout<<"refc="<<refc<<endl;
    oneInt=3; // 修改oneInt所在内存
    cout<<"ref="<<ref<<endl;
    cout<<"refc="<<refc<<endl;
    int& ref2=ref; //为普通变量ref设置别名(普通引用)，ref2指向与ref同一块内存空间
    cout<<"ref2="<<ref2<<endl;
    cout<<"oneInt="<<oneInt;
    // refc=5 修改会报错，不可以通过常引用修改内存数据
    return 0;

}
