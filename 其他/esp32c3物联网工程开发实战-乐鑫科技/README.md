<!--
 * @Author: Wj
 * @Date: 2023-11-11 18:38:28
 * @LastEditors: Wj
 * @LastEditTime: 2023-11-11 18:38:33
 * @Description: 请填写简介
-->
<!--
 * @Author: Wj
 * @Date: 2023-11-11 18:36:31
 * @LastEditors: Wj
 * @LastEditTime: 2023-11-11 18:36:31
 * @Description: 请填写简介
-->
<!--
 * @Author: Wj
 * @Date: 2023-11-10 08:37:15
 * @LastEditors: Wj
 * @LastEditTime: 2023-11-11 18:35:44
 * @Description: 请填写简介
-->
# 进度
```mermaid
gantt
title 2023页码进度🌰

dateFormat MM-DD

section 页码进度
82:crit,des1,11-12,1d
```
```mermaid
gantt
title 2023章节进度🌰

dateFormat MM-DD

section 章节进度
第一篇_准备篇:crit,des1,11-12,1d
第二篇_硬件与驱动开发:crit,des1,11-12,1d
```
# 卡壳位置
第二篇_硬件与驱动开发

原因：文件太复杂不直观，而且结构太多，重点是对rtos的驱动开发不熟悉

对rtos不熟悉，没有接触过，这个不适合入门

而且自己没有裸机编程经验，说白了就是没有基础

# 收获
了解了自己知识的不足，并且知道了所有的上层都是基于下层的封装

从而提供初始化的api以及功能的api

坚定了学习好操作系统，c语言，c++以及数学的决心，以及各种基础课程的决

心，也知道了不能自己闭门造车，也要多多的去了解和接收一些相关培训

除了看官网还有很多的库，一定要掌握读源码的能力以及模仿的能力，需要大量

的学习小的demo，并且要深入到协议学习，而不是api级别的，要深入到原理学

习，然后自己尝试封装api

基础是很困难的，要学会自己去研究和模仿，选好自己喜欢的方向，深入的学习和研究

也明白了看不懂一定要多讨论然后看相关的demo逐步的研究透彻