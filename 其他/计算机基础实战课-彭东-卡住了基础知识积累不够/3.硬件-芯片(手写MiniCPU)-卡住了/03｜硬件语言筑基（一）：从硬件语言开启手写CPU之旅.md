<!--
 * @Author: Wj
 * @Date: 2023-11-11 08:45:37
 * @LastEditors: Wj
 * @LastEditTime: 2023-11-11 09:28:31
 * @Description: 请填写简介
-->
![img](https://static001.geekbang.org/resource/image/3c/3b/3c36d7832261f4cc35d4c142c17ddd3b.jpg?wh=1920x1026)

```verilog
module counter(
  //端口定义
  input                   reset_n,  //复位端，低有效
  input                   clk,       //输入时钟
  output [3:0]            cnt,      //计数输出
  output                  cout     //溢出位
);  
  
reg [3:0]               cnt_r ;      //计数器寄存器
  
always@(posedge clk or negedge reset_n) begin
  if(!reset_n) begin                  //复位时，计时归0
      cnt_r        <= 4'b0000 ;
    end
    else if (cnt_r==4'd9) begin      //计时10个cycle时，计时归0
      cnt_r        <=4'b0000;
    end
  else begin                      
      cnt_r        <= cnt_r + 1'b1 ; //计时加1
  end
end
  
  assign  cout = (cnt_r==4'd9) ;       //输出周期位
  assign  cnt  = cnt_r ;               //输出实时计时器
  
endmodule
```

![img](https://static001.geekbang.org/resource/image/13/bf/13ec949bd7c269421b2cae33381c49bf.jpg?wh=1920x1397)