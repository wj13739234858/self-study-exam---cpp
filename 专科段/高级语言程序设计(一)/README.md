<!--
 * @Author: Wj
 * @Date: 2023-11-11 18:36:31
 * @LastEditors: Wj
 * @LastEditTime: 2023-11-11 18:36:31
 * @Description: 请填写简介
-->
<!--
 * @Author: Wj
 * @Date: 2023-11-10 08:37:15
 * @LastEditors: Wj
 * @LastEditTime: 2023-11-11 18:35:44
 * @Description: 请填写简介
-->
# 进度
```mermaid
gantt
title 2023页码进度🌰

dateFormat MM-DD

section 页码进度
10:crit,des1,11-10,1d
20:crit,des1,11-11,1d
```
```mermaid
gantt
title 2023章节进度🌰

dateFormat MM-DD

section 章节进度
10:crit,des1,11-10,1d
20:crit,des1,11-11,1d
```