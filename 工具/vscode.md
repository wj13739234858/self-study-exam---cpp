# 插件
```
C/C++ Extension Pack  c++支持插件
Chinese (Simplified) (简体中文) Language  简中支持插件
Markdown Preview Mermaid Support  markdown甘特图语法支持插件
vscode-icons 图标插件
koroFileHeader 注释插件
```
## koroFileHeader 注释插件
### 创建文件自动注释
window：ctrl+win+i
### 函数注释
window：ctrl+alt+t
# 快捷键
```
ctrl+[/]  整体缩进
```
# 参考
https://blog.csdn.net/hzxOnlineOk/article/details/130866803 - 自动注释插件