# 工具环境
vscode + 插件
## 参考
https://zhuanlan.zhihu.com/p/139140492#%E8%AF%AD%E6%B3%95%E6%89%A9%E5%B1%95

# 基础语法
## 参考
https://markdown.com.cn/cheat-sheet.html#%E6%80%BB%E8%A7%88 - 官方教程
# 基础语法
## 任务列表
- [x] Write the press release
# 甘特图
插件：Markdown Preview Mermaid Support
## 例程

```mermaid
gantt
title 这是个甘特图的栗子🌰

dateFormat MM-DD

section 软件协同开发课程
项目启动 :done,des1,03-09,7d
项目计划 :done,des2,after des1,6d
需求分析 :done,des3,after des2,9d
软件设计 :done,des4,after des3,12d
软件编码 :crit,active,des5,04-07,20d
软件测试 :des6,04-14,15d
项目交付 :des7,after des6,4d
```
# 
## 参考

https://blog.csdn.net/weixin_43207777/article/details/106331431

# 目录结构
windows 命令tree
```
tree app /f
```
## 参考
https://zhuanlan.zhihu.com/p/649946235 - markdown结合tree
https://zhuanlan.zhihu.com/p/144616508 - linux
https://blog.csdn.net/SilenceJude/article/details/99673949 - windows